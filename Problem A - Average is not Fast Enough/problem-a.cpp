#include <iostream>

using namespace std;

// stores information about a single team
struct Team {
	int teamNumber;
	int sectionTimes[20]; // 20 is maximum number of sections
	bool disqualified;
};

int main() {
	int numSections; // number of segments to the race
	float totDist; // distance of all segments together
	cin >> numSections >> totDist;
	int totNumTeams = 0;
	Team teams[999]; // maximum allowed, but probably never so many
	int ibuf; // for team numbers
	char cbuf[7]; // for times
	while (cin >> ibuf) {
		teams[totNumTeams].teamNumber = ibuf;
		teams[totNumTeams].disqualified = false; // assume all good times
		// loop over all segment times while team isn't disqualified
		for (int i = 0; i < numSections && !teams[totNumTeams].disqualified; i++) {
			cin >> cbuf; // 0:00:00 -:--:--
			if (cbuf[0] == '-') { // indicates disqualified
				teams[totNumTeams].disqualified = true;
			}
			else { // good time
				int hours = cbuf[0] - '0';
				int minutes = (cbuf[2] - '0')*10 + (cbuf[3] - '0');
				int seconds = (cbuf[5] - '0')*10 + (cbuf[6] - '0');
				// store time in seconds
				teams[totNumTeams].sectionTimes[i] = hours*60*60 + minutes*60 + seconds;
			}
		}
		// always increment number of teams
		totNumTeams++;
	}
	// loop over all teams
	for (int i = 0; i < totNumTeams; i++) {
		// output team number, right aligned
		if (teams[i].teamNumber < 100) cout << " ";
		if (teams[i].teamNumber < 10)  cout << " ";
		cout << teams[i].teamNumber << ": ";
		// display hyphen if disqualified
		if (teams[i].disqualified) {
			cout << "-";
		}
		// otherwise do harder stuff
		else {
			int totTime = 0;
			// add up section times to a total
			for (int j = 0; j < numSections; j++)
			{
				totTime += teams[i].sectionTimes[j];
			}
			// rate is time/distance
			float totRate = (float)totTime / totDist;
			int minutes = totRate / 60; // whole part only
			float leftover = (totRate/60) - minutes; // fractional leftovers
			int seconds = leftover * 60; // seconds are fractional leftovers * 60
			// see if we need to round seconds up
			leftover = (leftover*60)-(int)(leftover*60); 
			if (leftover > .5) seconds++;
			// see if we have too many seconds
			if (seconds > 59) {
				minutes++;
				seconds -= 60;
			}
			// finally display rate
			cout << minutes << ":";
			if (seconds < 10) cout << "0";
			cout << seconds << " min/km";
		}
		cout << endl;
	}	
	return 0;
}